#!/usr/bin/env python

import string
import socket
import SocketServer
import paramiko
import threading
import time
import select
import re

"""
FTP and SSH server settings
"""
SERVER =
CONTROL_PORT =
SSH_LOGIN =
SSH_PW =
FTP_LOGIN =
FTP_PW = 

"""
User preferences
"""
CONTROL_SRCPORT = 2001
DATA_SRCPORT = 2002

class ForwardServer(SocketServer.ThreadingTCPServer):
    """
    FROM /paramiko/blob/master/demos/forward.py
    """
    daemon_threads = True
    allow_reuse_address = True
    
    #Own hack to get the fucking thing to shut itself down
    def handle_error(self, request, client_address):
        self.shutdown()
        #print("Thread closed")
	
class Handler(SocketServer.BaseRequestHandler):
    """
    FROM /paramiko/blob/master/demos/forward.py
    """
    def handle(self):
        try:
            chan = self.ssh_transport.open_channel("direct-tcpip", (self.chain_host, self.chain_port), self.request.getpeername())
        except Exception, e:
            print("Incoming request to %s:%d failed: %s" % (self.chain_host, self.chain_port, repr(e)))
            return
        if chan is None:
            print("Incoming request to %s:%d was rejected by the SSH server." % (self.chain_host, self.chain_port))
            return
		
        #print("Connected! Tunnel open %r -> %r -> %r" % (self.request.getpeername(), chan.getpeername(), (self.chain_host, self.chain_port)))
		
        while True:
            r, w, x = select.select([self.request, chan], [], [])
            if self.request in r:
                data = self.request.recv(1024)
                if len(data) == 0:
                    break
                chan.send(data)
            if chan in r:
                data = chan.recv(1024)
                if len(data) == 0:
                    break
                self.request.send(data)
				
        peername = self.request.getpeername()
        chan.close()
        self.request.close()
        #print("Tunnel closed from %r" % (peername,))

    def finish(self):
        if(self.tunnel_type == "control"):
            #print("Tunnel type is control, keeping it alive..")
            pass
        elif(self.tunnel_type == "data"):
            #print("Tunnel type is data, shutting down ssh connection..")
            self.ssh_client.close()
            #print("Tunnel closed, closing thread..")
            raise SystemExit
            
#def forward_tunnel(local_port, remote_host, remote_port, transport):
def forward_tunnel(local_port, remote_host, remote_port, transport, client, type):
    """
    FROM /paramiko/blob/master/demos/forward.py
    """
    class Subhander(Handler):
        chain_host = remote_host
        chain_port = remote_port
        ssh_transport = transport
        ssh_client = client#
        tunnel_type = type#
    ForwardServer(("", local_port), Subhander).serve_forever()
    #tunnel = ForwardServer(("", local_port), Subhander)
    #thread = threading.Thread(target=tunnel.serve_forever)
    #thread.daemon = True
    #thread.start()
    #return thread, tunnel

#def ssh_tunnel(server, ssh_login, ssh_pw, src_port, dst_port):
def ssh_tunnel(server, ssh_login, ssh_pw, src_port, dst_port, type):
    """
    This function sets up a SSH tunnel to ssh_login@server between src_port and dst_port. Type can be a string "control" or "data", control tunnel threads are designed to stay up forever as daemon, while data tunnel threads should close after one TCP exchange
    Args: server, ssh_login, ssh_pw, src_port, dst_port, type
    Returns: never returns, execute this in a thread!
    """
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy()) 
    #print("Connecting to ssh host %s:%d ..." % (server, 22))
    try:
        client.connect(server, 22, ssh_login, ssh_pw)
    except Exception, e:
        print("Failed to connect to %s:%d: %r" % (server, 22, e))
        sys.exit(1)
    #print("Now forwarding port %d to %s:%d ..." % (src_port, server, dst_port))
    try:
        #forward_tunnel(src_port, server, dst_port, client.get_transport())
        forward_tunnel(src_port, server, dst_port, client.get_transport(), client, type)
    #except KeyboardInterrupt:
    #    print "Ctrl-c: Port forwarding stopped"
    #    client.close()
    #    sys.exit(0)
    #thread, tunnel = forward_tunnel(src_port, server, dst_port, client.get_transport())
    #return thread, tunnel
    finally:
        client.close()

def ftp_init(server, dst_port, ftp_login, ftp_pw):
    """
    This function sets up an FTP control connection to server:dst_port, using ftp_login and ftp_pw
    Arguments: server, dst_port, ftp_login, ftp_pw
    Returns: FTP control connection (None if error occurred)
    """
    reg_hello = re.compile("220.*")
    reg_pw = re.compile("331.*")
    reg_welcome = re.compile("230.*")
    
    #control_tunnel = ssh_tunnel(server, SSH_LOGIN, SSH_PW, CONTROL_SRCPORT, dst_port)
    control_thread = threading.Thread(target = ssh_tunnel, args = (SERVER, SSH_LOGIN, SSH_PW, CONTROL_SRCPORT, CONTROL_PORT, "control", ))
    control_thread.daemon = True
    control_thread.start()
    time.sleep(0.1)

    control = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        control.connect(("127.0.0.1", CONTROL_SRCPORT))
        time.sleep(0.1)
    except Exception, e:
        print(e)
        return None
    
    reply = control.recv(4096)
    if(reg_hello.match(reply)):
        #print(reply)
        control.sendall("USER {0}\r\n".format(ftp_login))
        reply = control.recv(4096)
        if(reg_pw.match(reply)):
            #print(reply)
            control.sendall("PASS {0}\r\n".format(ftp_pw))
            reply = control.recv(4096)
            if(reg_welcome.match(reply)):
                #print(reply)
                return control
            else:
                print("Wrong password! Contact your administrator!")
        else:
            print("Wrong username! Contact your administrator!")
    else:
        print("Got wrong server hello message! Contact your administrator!")
        
    control.close()
    return None
    #return (control_tunnel, control)

def ftp_data(control):
    """
    This function sets up an FTP data connection to the server, returning a connected data socket, and an SSH tunnel thread
    Arguments: control connection
    Returns: data, data_thread (after closing the data connection, wait on the data_thread to close down using join()!)
    """
    reg_typeok = re.compile("200.*")
    reg_ok = re.compile("2.*")
    reg_pasvok = re.compile("227.*")
    
    control.sendall("TYPE I\r\n")
    reply = control.recv(4096)
    if(reg_typeok.match(reply)):
        #print(reply)
        control.sendall("PASV\r\n")
        reply = control.recv(4096)
        if(reg_pasvok.match(reply)):
            #print(reply)
            start = string.find(reply, "(")
            end = string.find(reply, ")")
            reply = reply[start+1:end]
            numbers = string.split(reply, ",")
            high_byte = int(numbers[-2])
            low_byte = int(numbers[-1])
            port = (high_byte<<8) + low_byte
            
            #data_tunnel = ssh_tunnel(SERVER, SSH_LOGIN, SSH_PW, DATA_SRCPORT, port)
            data_thread = threading.Thread(target = ssh_tunnel, args = (SERVER, SSH_LOGIN, SSH_PW, DATA_SRCPORT, port, "data", ))
            data_thread.daemon = True
            data_thread.start()
            data = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            time.sleep(0.1)
            try:
                data.connect(("127.0.0.1", DATA_SRCPORT))
                time.sleep(0.1)
                return data, data_thread
            except Exception, e:
                print(e) 
        else:
            print("Changing to passive mode not supported! Contact your administrator!")
            print(reply)
    else:
        print("Changing type to binary not supported! Contact your administrator!")
        print(reply)
    
    return None

def ftp_list(control):
    """
    This function lists the current files and directories
    Arguments: control connection
    Returns: -
    """
    reg_connok = re.compile("125.*")
    reg_transferok = re.compile(".*226.*") 
    reg_ok = re.compile("2.*")

    data, data_thread = ftp_data(control)
    if(data is None):
        print("Could not establish a data connection! Contact your administrator!")
        return
        
    control.sendall("LIST\r\n")
    reply = control.recv(4096)
    #if(reg_transferok.match(reply)):
    #if(reg_connok.match(reply)):
    if(reg_ok.match(reply) or reg_connok.match(reply)):
        #print(reply)
        response = data.recv(2048)
        print(">FILES:")
        print(response)
        data.close()
        data_thread.join()
        #ssh_exit(data_tunnel)
        control.settimeout(0.1)
        try:
            reply = control.recv(4096)
            #print("Extra data: {0}".format(reply))
        except Exception, e:
            #print("Nothing more recvd")
            pass
        control.settimeout(None)
    else:
        print("Sending data unsuccesful! Contact your administrator!") 

def ftp_cwd(control, directory):
    """
    This function tries to change the PWD of the FTP connection to the directory supplied in the arguments
    Arguments: control, directory
    Returns: -
    """
    reg_pathnok = re.compile("5.*")
    reg_pathok = re.compile("250.*")
    reg_ok = re.compile("2.*")

    control.sendall("CWD {0}\r\n".format(directory))
    reply = control.recv(4096)
    if(reg_pathnok.match(reply)):
        print("Invalid dir!")
    #elif(reg_pathok.match(reply)):
    elif(reg_ok.match(reply)):
        print(reply)
    else:
        print("Unsuspected answer: {0}. Contact your administrator!".format(reply))

def ftp_dl(control, f):
    """
    This function attempts to download the file f, which gets saved in the PWD of this script
    Arguments: control, f
    Returns: -
    """
    reg_filenamenok = re.compile("55.*")
    reg_connok = re.compile("125.*")
    reg_transferok = re.compile(".*226.*")
    reg_ok = re.compile("2.*")

    data, data_thread = ftp_data(control)
    if(data is None):
        print("Could not establish a data connection! Contact your administrator!")
        return
    
    control.sendall("RETR {0}\r\n".format(f))
    reply = control.recv(4096)
    if(reg_filenamenok.match(reply)):
        print("Invalid filename!")
    elif(reg_connok.match(reply)):
        new_file = open(f, 'wb+')
        response = data.recv(2048)
        while(len(response) != 0):
            new_file.write(response)
            response = data.recv(2048)
        new_file.close()
        print("Stored file!")
    else:
        print("Unsuspected answer: {0}. Contact your administrator!".format(reply))

    data.close()
    data_thread.join()
    control.settimeout(0.1)
    try:
        reply = control.recv(4096)
        #print("Extra data: {0}".format(reply))
    except Exception, e:
        #print("Nothing more recvd")
        pass
    control.settimeout(None)

def ftp_ul(control, f):
    """
    This function attempts to upload the file f, which must be present in the PWD of this script
    Arguments: control, f
    Returns: -
    """
    reg_storenok = re.compile("5.*")
    reg_connok = re.compile("125.*")
    reg_transferok = re.compile(".*226.*") 

    data, data_thread = ftp_data(control)
    if(data is None):
        print("Could not establish a data connection! Contact your administrator!")
        return
    
    control.sendall("STOR {0}\r\n".format(f))
    reply = control.recv(4096)
    if(reg_storenok.match(reply)):
        print("Could not create file!")
    elif(reg_connok.match(reply)):
        send_file = open(f, 'rb')
        upload = send_file.read()
        data.send(upload)
        send_file.close()
        print("Uploaded file!")
    else:
        print("Unsuspected answer: {0}. Contact your administrator!".format(reply))
    
    data.close()
    data_thread.join()
    control.settimeout(0.1)
    try:
        reply = control.recv(4096)
        #print("Extra data: {0}".format(reply))
    except Exception, e:
        #print("Nothing more recvd")
        pass
    control.settimeout(None)

def ftp_mkdir(control, directory):
    """
    This function tries to make a new directory in the PWD of the FTP server
    Arguments: control, directory
    Returns: -
    """
    reg_dirnok = re.compile("5.*")
    reg_dirok = re.compile("2.*")

    control.sendall("MKD {0}\r\n".format(directory))
    reply = control.recv(4096)
    if(reg_dirnok.match(reply)):
        print("Failed making directory {0}".format(directory))
    elif(reg_dirok.match(reply)):
        print("Made directory!")
    else:
        print("Unsuspected answer: {0}. Contact your administrator!".format(reply))
        
    control.settimeout(0.1)
    try:
        reply = control.recv(4096)
        #print("Extra data: {0}".format(reply))
    except Exception, e:
        #print("Nothing more recvd")
        pass
    control.settimeout(None)

def ftp_del(control, filename):
    """
    This function tries to delete the file filename from the FTP server
    Arguments: control, filename
    Returns: -
    """
    reg_delnok = re.compile("5.*")
    reg_delok = re.compile("2.*")

    control.sendall("DELE {0}\r\n".format(filename))
    reply = control.recv(4096)
    if(reg_delnok.match(reply)):
        print("Failed deleting file {0}".format(filename))
    elif(reg_delok.match(reply)):
        print("Deleted file!")
    else:
        print("Unsuspected answer: {0}. Contact your administrator!".format(reply))
        
    control.settimeout(0.1)
    try:
        reply = control.recv(4096)
        #print("Extra data: {0}".format(reply))
    except Exception, e:
        #print("Nothing more recvd")
        pass
    control.settimeout(None)

def ftp_rmdir(control, directory):
    """
    This function attempts to delete the folder directory from the FTP server
    Arguments: control, directory
    Returns: -
    """
    reg_delnok = re.compile("5.*")
    reg_delok = re.compile("2.*")

    control.sendall("RMD {0}\r\n".format(directory))
    reply = control.recv(4096)
    if(reg_delnok.match(reply)):
        print("Failed removing directory {0}".format(directory))
    elif(reg_delok.match(reply)):
        print("Removed directory!")
    else:
        print("Unsuspected answer: {0}. Contact your administrator!".format(reply))
        
    control.settimeout(0.1)
    try:
        reply = control.recv(4096)
        #print("Extra data: {0}".format(reply))
    except Exception, e:
        #print("Nothing more recvd")
        pass
    control.settimeout(None)

def ftp_rename(control, oldname, newname):
    """
    This function attempts to rename the file oldname to newname on the FTP server
    Arguments: control, oldname, newname
    Returns: -
    """
    reg_renamenok = re.compile("5.*")
    reg_renametentok = re.compile("350.*")
    reg_renameok = re.compile("2.*")

    control.sendall("RNFR {0}\r\n".format(oldname))
    reply = control.recv(4096)
    if(reg_renamenok.match(reply)):
        print("Failed renaming file {0}".format(oldname))
    elif(reg_renametentok.match(reply)):
        control.sendall("RNTO {0}\r\n".format(newname))
        reply = control.recv(4096)
        if(reg_renamenok.match(reply)):
            print("Failed renaming file to {0}".format(newname))
        elif(reg_renameok.match(reply)):
            print("Renamed file!")
        else:
            print("Unsuspected answer: {0}. Contact your administrator!".format(reply))
    else:
        print("Unsuspected answer: {0}. Contact your administrator!".format(reply))
        
    control.settimeout(0.1)
    try:
        reply = control.recv(4096)
        #print("Extra data: {0}".format(reply))
    except Exception, e:
        #print("Nothing more recvd")
        pass
    control.settimeout(None)

def ftp_quit(control):
    """
    This function closes the FTP connection to the server gracefully
    Arguments: control_tunnel, control
    Returns: -
    """
    reg_bye = re.compile("221.*")

    control.sendall("QUIT\r\n")
    reply = control.recv(4096)
    if(reg_bye.match(reply)):
        print(reply)
    else:
        print("Got wrong server bye message! Contact your administrator!")
        
    control.close()

def main():
    """
    Experimental FTP Client
    """
    print("Welcome to the FTP over SSH Client!")
    #control_tunnel, control = ftp_init(SERVER, CONTROL_PORT, FTP_LOGIN, FTP_PW)
    control = ftp_init(SERVER, CONTROL_PORT, FTP_LOGIN, FTP_PW)
    print("Connection established")
    while(True):
        ftp_list(control)
        inputstr = raw_input("What would you like to do?\n\t1. Change directory\n\t2. Download file\n\t3. Upload file\n\t4. Make directory\n\t5. Delete file\n\t6. Delete directory\n\t7. Rename file\n\t9. Quit\n>")
        if(inputstr == "1"):
            directory = raw_input("Directory?\n>")
            ftp_cwd(control, directory)
        elif(inputstr == "2"):
            filename = raw_input("File?\n>")
            ftp_dl(control, filename)
        elif(inputstr == "3"):
            filename = raw_input("File?\n>")
            ftp_ul(control, filename)
        elif(inputstr == "4"):
            directory = raw_input("Directory?\n>")
            ftp_mkdir(control, directory)
        elif(inputstr == "5"):
            filename = raw_input("File?\n>")
            ftp_del(control, filename)
        elif(inputstr == "6"):
            directory = raw_input("Directory?\n>")
            ftp_rmdir(control, directory)
        elif(inputstr == "7"):
            oldname = raw_input("File you wish to rename?\n>")
            newname = raw_input("New name for file {0}?\n>".format(oldname))
            ftp_rename(control, oldname, newname)
        elif(inputstr == "9"):
            ftp_quit(control)
            print("Goodbye!")
            break
        else:
            print(inputstr)
            print("Not a valid command!")

if __name__ == "__main__":
    main()

