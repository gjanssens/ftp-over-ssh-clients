FTP over SSH clients for Linux and Windows. Get secure access to FTP servers that also run an SSH server, without having to change anything at the server side!

These clients are provided as is, without guarantees. Written as proof-of-concept, now available to whomever wants to use the code as a starting point, or for some general ideas on how to implement a secure solution for a deprecated FTP server.

To try it out:

If you run a decent (POSIX) OS, you can run FTPClient.py without much prerequisites. Tested on a 64bit Ubuntu 13.04 machine.

If you inist on running Windows, you can run FTPWinClient.py, which was tested with the following dependencies on a 32bit Windows XP VM:

- Python2.7

- Python for windows essentials

- paramiko library (which in turn needs the PyCrypto library, search around for precompiled binaries, this is the easiest way)

FTP/SSH server IP and credentials need to be provided in the python clients!
