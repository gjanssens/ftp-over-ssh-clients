#!/usr/bin/env python

import socket
import time
import pexpect
import string

"""
FTP and SSH server settings
"""
SERVER =
CONTROL_PORT =
SSH_LOGIN =
SSH_PW =
FTP_LOGIN =
FTP_PW =

"""
User preferences
"""
CONTROL_SRCPORT = 2001
DATA_SRCPORT = 2002

def ssh_tunnel(server, ssh_login, ssh_pw, src_port, dst_port):
    """
    This function sets up a SSH tunnel to ssh_login@server between src_port and dst_port
    Args: server, ssh_login, ssh_pw, src_port, dst_port
    Returns: the child, after you've finished with this tunnel, you need to quit the connection!
    """
    child = pexpect.spawn("ssh -L{0}:{1}:{2} {3}@{1}".format(src_port, server, dst_port, ssh_login))
    #print("ssh -L{0}:{1}:{2} {3}@{1}".format(src_port, server, dst_port, ssh_login))
    i = child.expect(["Are you sure you want to continue connecting", "password:", pexpect.EOF, pexpect.TIMEOUT])
    if i==0:
        #print("Authenticating host")
        child.sendline("yes")
        i = child.expect(["Are you sure you want to continue connecting", "password:", pexpect.EOF, pexpect.TIMEOUT])
    if i==1:
        #print("Sending password")
        child.sendline(ssh_pw)
        if child.expect(["[#\$] ", "Permission denied"]) == 0:
            #print("Tunnel ready")
            #print("ssh -L{0}:{1}:{2} {3}@{1} ready.".format(src_port, server, dst_port, ssh_login))
            return child
        else:
            print("Permission denied!")
    elif i==2:
        print("Connection timeout")
        
    child.kill(0)

def ssh_exit(child):
    """
    This function closes the SSH tunnel controlled by child
    Arguments: pexpect child
    Returns: -
    """
    #child.sendline("exit")
    #child.expect("logout")
    child.kill(0)

def ftp_init(server, dst_port, ftp_login, ftp_pw):
    """
    This function sets up an FTP control connection to server:dst_port, using ftp_login and ftp_pw
    Arguments: server, dst_port, ftp_login, ftp_pw
    Returns: FTP control tunnel, FTP control connection
    """
    control_tunnel = ssh_tunnel(server, SSH_LOGIN, SSH_PW, CONTROL_SRCPORT, dst_port)

    control = pexpect.spawn("telnet localhost {0}".format(CONTROL_SRCPORT))
    control.expect("220.*")
    control.sendline("USER {0}".format(ftp_login))
    control.expect("331 Username ok, send password.")
    control.sendline("PASS {0}".format(ftp_pw))
    control.expect("230.*")

    return (control_tunnel, control)

def ftp_data(control):
    """
    This function sets up an FTP data connection to the server, returning an SSH tunnel child, and a connected data socket
    Arguments: control connection
    Returns: data_tunnel, data (both objects must be closed after the data connection is finished!)
    """
    control.sendline("TYPE I")
    control.expect("200 Type set to: Binary.")
    control.sendline("PASV")
    control.expect("227 Entering passive mode \(\d\d?\d?,\d\d?\d?,\d\d?\d?,\d\d?\d?,\d\d?\d?,\d\d?\d?\).")
    reply = control.after
    start = string.find(reply, "(")
    end = string.find(reply, ")")
    reply = reply[start+1:end]
    numbers = string.split(reply, ",")
    high_byte = int(numbers[-2])
    low_byte = int(numbers[-1])
    port = (high_byte<<8) + low_byte
    
    data_tunnel = ssh_tunnel(SERVER, SSH_LOGIN, SSH_PW, DATA_SRCPORT, port)
    data = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        data.connect(("127.0.0.1", DATA_SRCPORT))
        time.sleep(0.1)
        return data, data_tunnel
    except Exception, e:
        ssh_exit(data_tunnel)
        print(e)

def ftp_list(control):
    """
    This function lists the current files and directories
    Arguments: control connection
    Returns: -
    """
    data, data_tunnel = ftp_data(control)

    control.sendline("LIST")
    control.expect("226 Transfer complete.")

    response = data.recv(2048)
    print(">FILES:")
    print(response)
    data.close()
    ssh_exit(data_tunnel)

def ftp_cwd(control, directory):
    """
    This function tries to change the PWD of the FTP connection to the directory supplied in the arguments
    Arguments: control, directory
    Returns: -
    """
    control.sendline("CWD {0}".format(directory))
    #i = control.expect(["550 Invalid database path", '250 "[/. //0-9a-zA-Z]*" is the current directory.'])
    i = control.expect(["55.*", '250 "[ //0-9a-zA-Z]*" is the current directory.'])
    if i==0:
        print("Invalid dir!")
    else:
        response = control.after
        print(response)

def ftp_dl(control, f):
    """
    This function attempts to download the file f, which gets saved in the PWD of this script
    Arguments: control, f
    Returns: -
    """
    data, data_tunnel = ftp_data(control)

    control.sendline("RETR {0}".format(f))
    i = control.expect(["550 .*", "226 Transfer complete."])
    if i==0:
        print("Invalid filename!")
    else:
        new_file = open(f, 'w')
        response = data.recv(2048)
        while(len(response) != 0):
            new_file.write(response)
            response = data.recv(2048)
        new_file.close()
        print("Stored file!")

    data.close()
    ssh_exit(data_tunnel)

def ftp_ul(control, f):
    """
    This function attempts to upload the file f, which must be present in the PWD of this script
    Arguments: control, f
    Returns: -
    """
    data, data_tunnel = ftp_data(control)

    control.sendline("STOR {0}".format(f))
    i = control.expect(["5.*", "2.*"])
    if i==0:
        print("Could not create file!")
    else:
        send_file = open(f, 'rb')
        upload = send_file.read()
        data.send(upload)
        send_file.close()
        print("Uploaded file!")

    data.close()
    ssh_exit(data_tunnel)

def ftp_mkdir(control, directory):
    """
    This function tries to make a new directory in the PWD of the FTP server
    Arguments: control, directory
    Returns: -
    """
    control.sendline("MKD {0}".format(directory))
    i = control.expect(["5.*", "2.*"])
    if i==0:
        print("Failed making directory {0}".format(directory))
    else:
        print("Made directory!")

def ftp_del(control, filename):
    """
    This function tries to delete the file filename from the FTP server
    Arguments: control, filename
    Returns: -
    """
    control.sendline("DELE {0}".format(filename))
    i = control.expect(["5.*", "2.*"])
    if i==0:
        print("Failed deleting file {0}".format(filename))
    else:
        print("Deleted file!")

def ftp_rmdir(control, directory):
    """
    This function attempts to delete the folder directory from the FTP server
    Arguments: control, directory
    Returns: -
    """
    control.sendline("RMD {0}".format(directory))
    i = control.expect(["5.*", "2.*"])
    if i==0:
        print("Failed removing directory {0}".format(directory))
    else:
        print("Removed directory!")

def ftp_rename(control, oldname, newname):
    """
    This function attempts to rename the file oldname to newname on the FTP server
    Arguments: control, oldname, newname
    Returns: -
    """
    control.sendline("RNFR {0}".format(oldname))
    i = control.expect(["5.*", "350.*"])
    if i==0:
        print("Failed renaming file {0}".format(oldname))
    else:
        control.sendline("RNTO {0}".format(newname))
        j = control.expect(["5.*", "2.*"])
        if j==0:
            print("Failed renaming file to {0}".format(newname))
        else:
            print("Renamed file!")

def ftp_quit(control_tunnel, control):
    """
    This function closes the FTP connection to the server gracefully
    Arguments: control_tunnel, control
    Returns: -
    """
    control.sendline("QUIT")
    control.expect("221 Bye.")
    control.kill(0)
    ssh_exit(control_tunnel)

def main():
    """
    Experimental FTP Client
    """
    print("Welcome to the FTP over SSH Client!")
    control_tunnel, control = ftp_init(SERVER, CONTROL_PORT, FTP_LOGIN, FTP_PW)
    print("Connection established")
    while(True):
        ftp_list(control)
        inputstr = raw_input("What would you like to do?\n\t1. Change directory\n\t2. Download file\n\t3. Upload file\n\t4. Make directory\n\t5. Delete file\n\t6. Delete directory\n\t7. Rename file\n\t9. Quit\n>")
        if(inputstr == "1"):
            directory = raw_input("Directory?\n>")
            ftp_cwd(control, directory)
        elif(inputstr == "2"):
            filename = raw_input("File?\n>")
            ftp_dl(control, filename)
        elif(inputstr == "3"):
            filename = raw_input("File?\n>")
            ftp_ul(control, filename)
        elif(inputstr == "4"):
            directory = raw_input("Directory?\n>")
            ftp_mkdir(control, directory)
        elif(inputstr == "5"):
            filename = raw_input("File?\n>")
            ftp_del(control, filename)
        elif(inputstr == "6"):
            directory = raw_input("Directory?\n>")
            ftp_rmdir(control, directory)
        elif(inputstr == "7"):
            oldname = raw_input("File you wish to rename?\n>")
            newname = raw_input("New name for file {0}?\n>".format(oldname))
            ftp_rename(control, oldname, newname)
        elif(inputstr == "9"):
            ftp_quit(control_tunnel, control)
            print("Goodbye!")
            break
        else:
            print(inputstr)
            print("Not a valid command!")

if __name__ == "__main__":
    main()

